import { useState, FormEvent } from 'react'
import { useHistory } from 'react-router-dom'
import { toast } from 'react-toastify'
import { ArrowBack } from '@styled-icons/boxicons-regular/ArrowBack'

import { formatToInt } from 'utils/formatToCurrency'
import * as mask from 'utils/mask'
import validateSchema from 'utils/validateSchema'

import Button from 'components/Button'
import TextField from 'components/TextField'

import * as S from './styled'
import schema from './schema'


type FormTypes = {
  amount?: string
  credit_card_cvv?: string
  credit_card_holder_name?: string
  credit_card_expiration_date?: string
  credit_card_number?: string
  buyer_document?: string
}

const sendTransaction = async (formValue: FormTypes) => {
  const body = {
    ...formValue,
    amount: formatToInt(formValue.amount!),
    buyer_document: formValue.buyer_document!.replaceAll(/\.|-/g, ''),
    credit_card_number: formValue.credit_card_number!.replaceAll(' ', '')
  }

  console.log(JSON.stringify(body))

  return await fetch('http://localhost:3000/transactions', {
    headers: {
      'Content-Type': 'application/json'
    },
    method: 'POST', 
    body: JSON.stringify(body)
  })
}

const TransactionForm = () => {
  const history = useHistory()

  const [form, setForm] = useState({
    amount: '',
    credit_card_cvv: '',
    credit_card_holder_name: '',
    credit_card_expiration_date: '',
    credit_card_number: '',
    buyer_document: ''
  })

  const [errors, setErrors] = useState<FormTypes>({})

  const goBack = () => {
    history.replace('/')
  }

  const onSubmitForm = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()

    try {
      const result = await validateSchema(schema, form)
      setErrors(result)

      if (Object.keys(result).length <= 0) {
        await sendTransaction(form)
        toast.success('Transação criada com sucesso!')
      }
    } catch (error) {
      toast.error('Ops! Parece que ocorreu algum problema :(')
    }

    goBack()
  }

  return (
    <S.Wrapper>
      <S.Header>
        <S.Back role="button" onClick={goBack}>
          <ArrowBack size={24} />
        </S.Back>
        <S.Title>Nova transação</S.Title>
      </S.Header>

      <S.Form onSubmit={onSubmitForm}>
        <TextField 
          name="credit_card_holder_name" 
          placeholder="Nome da pessoa compradora"
          value={form.credit_card_holder_name}
          error={errors.credit_card_holder_name}
          required
          onChange={(event) => 
            setForm({ ...form, credit_card_holder_name: event.target.value })
          }
        />
        <TextField 
          name="buyer_document" 
          placeholder="CPF"
          value={form.buyer_document}
          error={errors.buyer_document}
          required
          onChange={(event) => 
            setForm({...form, buyer_document: mask.cpf(event.target.value) })
          }/>
        <TextField 
          name="credit_card_number" 
          placeholder="Nº do cartão"
          value={form.credit_card_number}
          error={errors.credit_card_number}
          required
          onChange={(event) => 
            setForm({...form, credit_card_number: mask.creditCardNumber(event.target.value) })
          } />
        <S.Row>
          <TextField 
            name="credit_card_expiration_date" 
            placeholder="Data de expiração"
            value={form.credit_card_expiration_date}
            error={errors.credit_card_expiration_date}
            required
            onChange={(event) => 
              setForm({...form, credit_card_expiration_date: mask.creditCardExpirationDate(event.target.value) })
            } />
          <TextField 
            name="credit_card_cvv" 
            placeholder="CVV"
            value={form.credit_card_cvv}
            error={errors.credit_card_cvv}
            required
            onChange={(event) => 
              setForm({...form, credit_card_cvv: mask.creditCardCvv(event.target.value) })
            } />
        </S.Row>
        <TextField 
          name="amount"
          placeholder="Valor da transação"
          value={form.amount}
          error={errors.amount}
          required 
          onChange={(event) => 
            setForm({...form, amount: mask.currency(event.target.value) })
          } />

        <Button>Criar transação</Button>
      </S.Form>
    </S.Wrapper>
  )
}

export default TransactionForm
