import styled from 'styled-components'

export const Wrapper = styled.button`
  width: 100%;
  height: 48px;
  border: 0;
  background-color: #3F2787;
  box-shadow: 0px 4px 6px rgba(112, 82, 200, 0.3);
  border-radius: 8px;
  font-size: 16px;
  font-weight: 700;
  color: #ffffff;
  cursor: pointer;

  svg {
    width: 24px;
    height: 24px;
    margin: 0 8px;
  }
`
