import styled from 'styled-components'

import { formatDateToPtBR } from 'utils/formatDate'
import { formatToCurrency } from 'utils/formatToCurrency'

const checkStatus = (status: string) => {
  return status === 'paid' ? 'Paga' : 'Recusada'
}

export type TransactionItemProps = {
  credit_card_holder_name: string
  amount: number
  date: string
  status: string
}

/*
  Esse componente está incompleto...
  Usando o que está neste arquivo, como você implementaria o código que está faltando?
*/

const TransactionItem = ({
  credit_card_holder_name,
  amount,
  date,
  status
}: TransactionItemProps) => (
  <>
  </>
)

export default TransactionItem
