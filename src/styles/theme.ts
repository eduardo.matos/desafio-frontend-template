export default {
  colors: {
    primary: '#3F2787',
    secondary: '#65A300',
    background: '#F2F2F3',
    white: '#ffffff',
    border: '#8b8b92',
  }
} as const
