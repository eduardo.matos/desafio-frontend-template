import { BrowserRouter, Switch, Route } from 'react-router-dom'

import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import GlobalStyles from 'styles/global'
import Home from 'pages/Home'
import TransactionForm from 'pages/TransactionForm'

function App() {
  return (
    <>
      <GlobalStyles />
      <BrowserRouter>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/transacao">
            <TransactionForm />
          </Route>
        </Switch>
      </BrowserRouter>
      <ToastContainer
        position="bottom-center"
        hideProgressBar={false}
        autoClose={3000}
      />
    </>
  )
}

export default App;
